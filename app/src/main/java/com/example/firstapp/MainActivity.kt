package com.example.firstapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener()
        {
            var type=""
            when(randomizer())
            {
                true->type="ლუწია"
                false->type="კენტია"
            }
            Toast.makeText(this,"რიცხვი $type",Toast.LENGTH_LONG).show()

        }

    }


     fun randomizer(): Boolean
    {
        val rand=(1..101).random()
        println(rand)
        return (rand%2)==0;


    }
}
